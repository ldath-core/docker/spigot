# spigot

```bash
docker run -v "`pwd`/world:/var/lib/minecraft" registry.gitlab.com/ldath-core/docker/spigot:1.14.3
```

## Development

**Requirements:**

-   Java SDK installed

-   Build Tools downloaded:
    ```bash
    curl https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar -o BuildTools.jar
    ```
    
-   `Docker` and `Docker Compose` installed

-   Spigot JAR generated for the Dockerfile (choose version you plan to use with `SPIGOT_VERSION` environment variable)

    ```bash
    export SPIGOT_VERSION=1.14.3
    ./build.sh
    ```

**Using docker-compose.yml:**
        
First Build image:

```bash

docker-compose build

```

Second run start command:

```bash
docker-compose up
```

When started for the first time this will create `world` folder with some default data but will fail in the end with log similar to this:

```text
Recreating spigot_spigot_1 ... done
Attaching to spigot_spigot_1
spigot_1  | Loading libraries, please wait...
spigot_1  | Loaded 0 recipes
spigot_1  | [15:14:33 INFO]: Loaded 0 recipes
spigot_1  | [15:14:33 INFO]: Starting minecraft server version 1.13.2
spigot_1  | [15:14:33 INFO]: Loading properties
spigot_1  | [15:14:33 WARN]: server.properties does not exist
spigot_1  | [15:14:33 INFO]: Generating new properties file
spigot_1  | [15:14:33 WARN]: Failed to load eula.txt
spigot_1  | [15:14:33 INFO]: You need to agree to the EULA in order to run the server. Go to eula.txt for more info.
spigot_1  | [15:14:33 INFO]: Stopping server
spigot_1  | [15:14:33 INFO]: Saving worlds
spigot_spigot_1 exited with code 0

```

Please update generated `eula.txt` and change line with `eula=false` to `eula=true` and start server again with:

```bash
docker-compose up
```

## TODO

Sources to continue work on this image:

  * https://www.spigotmc.org/threads/deploying-minecraft-servers-using-docker-and-git.106001/
  * https://github.com/dlord/spigot-docker/blob/master/spigot
  * https://github.com/nimmis/docker-spigot
  
Interesting plugins:

  * https://www.spigotmc.org/resources/villager-bank.83092/
  * https://www.spigotmc.org/resources/villager-market.82965/
  * https://dev.bukkit.org/projects/vault
  * https://www.spigotmc.org/resources/%E2%9E%A2-frconomy-economy-plugin.77851/
  * https://www.spigotmc.org/resources/griefprevention.1884/
  * https://www.spigotmc.org/resources/blocklocker.3268/
