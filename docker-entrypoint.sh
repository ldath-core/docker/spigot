#!/usr/bin/env bash
set -e

if [ "${1:0:1}" = '-' ]; then
  set -- java "$@"
fi

if [ "$1" = 'java' -a "$(id -u)" = '0' ]; then
    PROJECT_JAR_PATH=$(find /opt/minecraft -maxdepth 1 -name \*.jar)
    JAVA_FLAGS=${JAVA_FLAGS:-"-server -XX:+UnlockExperimentalVMOptions"}

    set -- gosu minecraft "$JAVA_HOME/bin/java" $JAVA_FLAGS $JAVA_DEBUG_FLAGS -jar $PROJECT_JAR_PATH nogui $SPIGOT_OPTIONS
fi

# Allow the user to run arbitrarily commands like bash
exec "$@"
