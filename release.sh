#!/usr/bin/env sh

current_ver=$(find ./ -maxdepth 1 -mindepth 1 -name 'spigot-*.jar' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+')
if [ -z "$current_ver" ]
then
      current_ver=$(find ./ -maxdepth 1 -mindepth 1 -name 'spigot-*.jar' | grep -Eo '[0-9]+\.[0-9]+')
fi

echo "Find Spigot version: ${current_ver}"

docker build -t $CI_REGISTRY_IMAGE:$current_ver --no-cache .
docker push $CI_REGISTRY_IMAGE:$current_ver
