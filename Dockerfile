FROM openjdk:17-buster
MAINTAINER Arkadiusz Tułodziecki atulodzi@gmail.com

ENV GOSU_VERSION 1.14

RUN set -x \
    && apt-get update && apt-get install -y --no-install-recommends ca-certificates wget gnupg dirmngr bash && rm -rf /var/lib/apt/lists/* \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && for server in \
        hkp://p80.pool.sks-keyservers.net:80 \
        hkp://keyserver.ubuntu.com:80 \
        ha.pool.sks-keyservers.net \
        pgp.mit.edu \
    ; do \
        echo "Fetching GPG key $GPG_KEYS from $server"; \
        gpg --no-tty --batch --keyserver "$server" --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && found=yes && break; \
    done; \
    gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true

RUN groupadd -g 1000 minecraft && \
    useradd -g minecraft -u 1000 -r -M minecraft && \
    mkdir -p /opt/minecraft /var/lib/minecraft

COPY docker-entrypoint.sh /

COPY spigot-*.jar /opt/minecraft/

RUN chown -R minecraft:minecraft /opt/minecraft/

EXPOSE 25565

VOLUME ["/var/lib/minecraft"]

WORKDIR "/var/lib/minecraft"

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["java"]
