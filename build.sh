#!/usr/bin/env bash

# Cleaning for the development
rm craftbukkit-*.jar
rm spigot-*.jar

SPIGOT_VERSION=${SPIGOT_VERSION:-"latest"}

curl https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar -o BuildTools.jar
java -jar BuildTools.jar --rev ${SPIGOT_VERSION}
